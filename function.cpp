#include "function.h"

#include <sstream>
#include <iostream>

std::shared_ptr<Object> Closure::Apply(std::shared_ptr<Scope> /*scope*/,
                                       const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != params_.size()) {
        std::stringstream ss;
        ss << "Invalid number of arguments. Expected " << params_.size() << ", found "
           << args.size();
        throw std::runtime_error(ss.str());
    }

    for (size_t i = 0; i < params_.size(); ++i) {
        auto symbol = AsSymbol(params_[i]);
        if (!symbol) {
            throw std::runtime_error("Closure parameters must be symbols");
        }
        local_scope_->Set(symbol->GetName(), args[i]);
    }

    std::shared_ptr<Object> result;
    for (auto& expr : body_) {
        result = expr->Eval(local_scope_);
    }

    return result;
}

std::shared_ptr<Object> NumberPredicate::Apply(std::shared_ptr<Scope> /*scope*/,
                                               const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 1) {
        throw std::runtime_error("number? expected 1 argument");
    }
    return std::make_shared<Boolean>(IsNumber(args[0]));
}

std::shared_ptr<Object> BooleanPredicate::Apply(std::shared_ptr<Scope> /*scope*/,
                                                const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 1) {
        throw std::runtime_error("boolean? expected 1 argument");
    }
    return std::make_shared<Boolean>(std::dynamic_pointer_cast<Boolean>(args[0]) != nullptr);
}

std::shared_ptr<Object> SymbolPredicate::Apply(std::shared_ptr<Scope> /*scope*/,
                                               const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 1) {
        throw std::runtime_error("boolean? expected 1 argument");
    }
    return std::make_shared<Boolean>(std::dynamic_pointer_cast<Symbol>(args[0]) != nullptr);
}

std::shared_ptr<Object> PairPredicate::Apply(std::shared_ptr<Scope> /*scope*/,
                                             const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 1) {
        throw std::runtime_error("boolean? expected 1 argument");
    }
    return std::make_shared<Boolean>(std::dynamic_pointer_cast<Cell>(args[0]) != nullptr);
}

std::shared_ptr<Object> ListPredicate::Apply(std::shared_ptr<Scope> /*scope*/,
                                             const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 1) {
        throw std::runtime_error("boolean? expected 1 argument");
    }

    bool value = true;
    auto object = args[0];
    while (object) {
        auto cell = AsCell(object);
        if (!cell) {
            value = false;
            break;
        }
        object = cell->GetSecond();
    }
    return std::make_shared<Boolean>(value);
}

template <class T>
std::vector<std::shared_ptr<T>> CastTo(const std::vector<std::shared_ptr<Object>>& objects) {
    std::vector<std::shared_ptr<T>> casted;
    casted.reserve(objects.size());
    std::transform(objects.begin(), objects.end(), std::back_inserter(casted),
                   [](const auto& object) {
                       if (auto typed = std::dynamic_pointer_cast<T>(object); typed) {
                           return typed;
                       } else {
                           throw std::runtime_error("Cannot cast object");
                       }
                   });
    return casted;
}

std::shared_ptr<Object> Plus::Apply(std::shared_ptr<Scope> /*scope*/,
                                    const std::vector<std::shared_ptr<Object>>& args) {
    auto numbers = CastTo<Number>(args);
    int64_t result = 0;
    for (const auto& number : numbers) {
        result += number->GetValue();
    }
    return std::make_shared<Number>(result);
}

std::shared_ptr<Object> IntegerOp::Apply(std::shared_ptr<Scope> /*scope*/,
                                         const std::vector<std::shared_ptr<Object>>& args) {
    if (args.empty()) {
        throw std::runtime_error("Integer operation expected 1 or more arguments");
    }
    auto numbers = CastTo<Number>(args);
    int64_t result = numbers[0]->GetValue();
    for (auto it = std::next(numbers.begin()); it != numbers.end(); ++it) {
        result = op_(result, (*it)->GetValue());
    }
    return std::make_shared<Number>(result);
}

std::shared_ptr<Object> IntegerComparison::Apply(std::shared_ptr<Scope> /*scope*/,
                                                 const std::vector<std::shared_ptr<Object>>& args) {
    auto numbers = CastTo<Number>(args);
    bool result = true;
    for (size_t i = 1; i < numbers.size(); ++i) {
        result &= op_(numbers[i - 1]->GetValue(), numbers[i]->GetValue());
    }
    return std::make_shared<Boolean>(result);
}

std::shared_ptr<Object> Multiplies::Apply(std::shared_ptr<Scope> /*scope*/,
                                          const std::vector<std::shared_ptr<Object>>& args) {
    auto numbers = CastTo<Number>(args);
    int64_t result = 1;
    for (const auto& number : numbers) {
        result *= number->GetValue();
    }
    return std::make_shared<Number>(result);
}

std::shared_ptr<Object> Abs::Apply(std::shared_ptr<Scope> /*scope*/,
                                   const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 1) {
        throw std::runtime_error("Abs function expected 1 argument");
    }
    auto numbers = CastTo<Number>(args);
    return std::make_shared<Number>(std::abs(numbers[0]->GetValue()));
}
