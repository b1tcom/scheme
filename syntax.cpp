#include "syntax.h"

std::shared_ptr<Object> Quote::Apply(std::shared_ptr<Scope> /*scope*/,
                                     const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 1) {
        throw std::runtime_error("Quote accepts 1 argument");
    }
    return args[0];
}

std::shared_ptr<Object> If::Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 2 && args.size() != 3) {
        throw SyntaxError("If expected 2 or 3 arguments");
    }
    auto condition = args[0];
    auto true_branch = args[1];
    if (condition->Eval(scope)->IsTrue()) {
        return true_branch->Eval(scope);
    } else if (args.size() == 3) {
        return args[2]->Eval(scope);
    }
    return std::make_shared<Cell>();
}

std::shared_ptr<Object> Define::Apply(std::shared_ptr<Scope> scope,
                                      const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 2) {
        throw SyntaxError("Define requires 2 arguments");
    }

    if (IsCell(args[0])) {  // function definition
        auto name_and_params = CellToVector(AsCell(args[0]));
        if (name_and_params.empty()) {
            throw std::runtime_error("Expected function name as first list element");
        }
        auto name = AsSymbol(name_and_params[0]);
        if (!name) {
            throw std::runtime_error("Expected symbol as a function name");
        }
        auto params = std::vector(name_and_params.begin() + 1, name_and_params.end());
        auto body = std::vector(args.begin() + 1, args.end());
        scope->Set(name->GetName(), std::make_shared<Closure>(params, body, scope));
    } else {
        auto symbol = AsSymbol(args[0]);
        if (!symbol) {
            throw SyntaxError("First argument of define must be symbol");
        }

        scope->Set(symbol->GetName(), args[1]->Eval(scope));
    }
    return std::make_shared<Cell>();
}

std::shared_ptr<Object> LambdaCreation::Apply(std::shared_ptr<Scope> scope,
                                              const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() < 2) {
        throw SyntaxError(
            "lambda function expected at least 2 arguments: (lambda (params*) body+)");
    }

    auto params = AsCell(args[0]);
    if (args[0] && !params) {
        throw SyntaxError("Argument list must be a list");
    }

    return std::make_shared<Closure>(CellToVector(params),
                                     std::vector(args.begin() + 1, args.end()), scope);
}

std::shared_ptr<Object> Set::Apply(std::shared_ptr<Scope> scope,
                                   const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 2) {
        throw SyntaxError("Set requires 2 arguments");
    }

    auto symbol = AsSymbol(args[0]);
    if (!symbol) {
        throw SyntaxError("First argument of set must be symbol");
    }

    scope->Update(symbol->GetName(), args[1]->Eval(scope));
    return std::make_shared<Cell>();
}

std::shared_ptr<Object> Eval::Apply(std::shared_ptr<Scope> scope,
                                    const std::vector<std::shared_ptr<Object>>& args) {
    if (args.size() != 1) {
        throw SyntaxError("Eval requires 1 argument");
    }

    return args[0]->Eval(scope)->Eval(scope);
}

std::shared_ptr<Object> BooleanFold::Apply(std::shared_ptr<Scope> scope,
                                           const std::vector<std::shared_ptr<Object>>& args) {
    if (args.empty()) {
        return std::make_shared<Boolean>(initial_);
    }
    bool result = initial_;
    for (auto it = args.begin(); it != std::prev(args.end()); ++it) {
        const auto& arg = *it;
        result = op_(result, arg->Eval(scope)->IsTrue());
        if (result != initial_) {
            return std::make_shared<Boolean>(result);
        }
    }
    return args.back()->Eval(scope);
}
