#include "scheme.h"
#include "syntax.h"
#include "scope.h"

#include <tokenizer.h>

#include <sstream>
#include <functional>

Scheme::Scheme() : global_scope_(std::make_shared<Scope>(nullptr)) {
    global_scope_->Set("quote", std::make_shared<Quote>());
    global_scope_->Set("eval", std::make_shared<Eval>());
    global_scope_->Set("if", std::make_shared<If>());
    global_scope_->Set("define", std::make_shared<Define>());
    global_scope_->Set("set!", std::make_shared<Set>());
    global_scope_->Set("and", std::make_shared<BooleanFold>(std::logical_and<>(), true));
    global_scope_->Set("or", std::make_shared<BooleanFold>(std::logical_or<>(), false));

    global_scope_->Set(Boolean::kTrueString, std::make_shared<Boolean>(true));
    global_scope_->Set(Boolean::kFalseString, std::make_shared<Boolean>(false));

    global_scope_->Set("null?", std::make_shared<NullPredicate>());
    global_scope_->Set("number?", std::make_shared<NumberPredicate>());
    global_scope_->Set("boolean?", std::make_shared<BooleanPredicate>());
    global_scope_->Set("symbol?", std::make_shared<SymbolPredicate>());
    global_scope_->Set("pair?", std::make_shared<PairPredicate>());
    global_scope_->Set("list?", std::make_shared<ListPredicate>());

    global_scope_->Set("lambda", std::make_shared<LambdaCreation>());
    global_scope_->Set("list", std::make_shared<ListCreation>());
    global_scope_->Set("cons", std::make_shared<Cons>());
    global_scope_->Set("car", std::make_shared<Car>());
    global_scope_->Set("cdr", std::make_shared<Cdr>());
    global_scope_->Set("set-car!", std::make_shared<SetCar>());
    global_scope_->Set("set-cdr!", std::make_shared<SetCdr>());
    global_scope_->Set("list-ref", std::make_shared<ListRef>());
    global_scope_->Set("list-tail", std::make_shared<ListTail>());

    global_scope_->Set("not", std::make_shared<Not>());

    global_scope_->Set("+", std::make_shared<Plus>());
    global_scope_->Set("*", std::make_shared<Multiplies>());
    global_scope_->Set("abs", std::make_shared<Abs>());

    global_scope_->Set("-", std::make_shared<IntegerOp>(std::minus<int64_t>()));
    global_scope_->Set("/", std::make_shared<IntegerOp>(std::divides<int64_t>()));
    global_scope_->Set("min", std::make_shared<IntegerOp>(
                                  [](int64_t lhs, int64_t rhs) { return std::min(lhs, rhs); }));
    global_scope_->Set("max", std::make_shared<IntegerOp>(
                                  [](int64_t lhs, int64_t rhs) { return std::max(lhs, rhs); }));

    global_scope_->Set("=", std::make_shared<IntegerComparison>(std::equal_to<>()));
    global_scope_->Set("<", std::make_shared<IntegerComparison>(std::less<>()));
    global_scope_->Set("<=", std::make_shared<IntegerComparison>(std::less_equal<>()));
    global_scope_->Set(">", std::make_shared<IntegerComparison>(std::greater<>()));
    global_scope_->Set(">=", std::make_shared<IntegerComparison>(std::greater_equal<>()));
}

std::string Scheme::Evaluate(std::string expression) {
    std::stringstream ss;
    ss << std::move(expression);

    Tokenizer tokenizer(&ss);
    auto expr = Read(&tokenizer);
    if (!expr) {
        throw std::runtime_error("Cannot evaluate empty list");
    }
    if (!tokenizer.IsEnd()) {
        throw SyntaxError("Expected end of input");
    }

    expr = expr->Eval(global_scope_);
    return Print(expr);
}
