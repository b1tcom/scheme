#pragma once

#include <variant>
#include <istream>
#include <unordered_map>

struct SymbolToken {
    std::string name;

   bool operator==(const SymbolToken& token) const {
       return name == token.name;
   }
};

struct QuoteToken {
    bool operator==(const QuoteToken&) const {
        return true;
    }
};

struct DotToken {
    bool operator==(const DotToken&) const {
        return true;
    }
};

struct UnknownToken {
    bool operator==(const UnknownToken&) const {
        return true;
    }
};

enum class BracketToken { OPEN, CLOSE };

struct ConstantToken {
    int value;

    bool operator==(const ConstantToken& token) const {
        return value == token.value;
    }
};

using Token =
    std::variant<ConstantToken, BracketToken, QuoteToken, SymbolToken, DotToken, UnknownToken>;

class Tokenizer {
public:
    Tokenizer(std::istream* in) : in_(in) {
    }

    bool IsEnd() {
        SkipWs();
        return in_->peek() == EOF;
    }

    void Next() {
        token_ = UnknownToken{};
    }

    Token GetToken() {
        if (std::holds_alternative<UnknownToken>(token_)) {
            Consume();
        }
        return token_;
    }


private:
    const std::string k_delimiters_ = "()\'. \t\n";

    std::istream* in_;
    Token token_{UnknownToken{}};

    void Consume() {
        SkipWs();
        if (TryParseNumber()) {
            return;
        } else if (k_delimiters_.find(in_->peek()) == std::string::npos) {
            std::string str;
            while (in_->peek() != EOF && k_delimiters_.find(in_->peek()) == std::string::npos) {
                str += in_->get();
            }
            token_ = SymbolToken{str};
        } else {
            switch (in_->peek()) {
                case '(':
                    token_ = BracketToken::OPEN;
                    in_->get();
                    break;
                case ')':
                    token_ = BracketToken::CLOSE;
                    in_->get();
                    break;
                case '\'':
                    token_ = QuoteToken{};
                    in_->get();
                    break;
                case '.':
                    token_ = DotToken{};
                    in_->get();
                    break;
                default:
                    std::string str;
                    str += in_->get();
                    token_ = SymbolToken{str};
            }
        }
    }

    bool TryParseNumber() {
        int number, sign = 1;
        bool unget = false;

        if (in_->peek() == '+' || in_->peek() == '-') {
            sign = in_->get() == '-' ? -1 : 1;
            unget = true;
        }
        if (isdigit(in_->peek())) {
            *in_ >> number;
            token_ = ConstantToken{sign * number};
            return true;
        }

        if (unget) {
            in_->unget();
        }
        return false;
    }

    void SkipWs() {
        while (isspace(in_->peek())) {
            in_->get();
        }
    }

    bool TryParseCharTokens() {
        std::unordered_map<char, Token> char_tokens = {
            {'(', BracketToken::OPEN},
            {')', BracketToken::CLOSE},
            {'.', DotToken{}},
            {'\'', QuoteToken{}}
        };

        auto it = char_tokens.find(in_->peek());
        if (it != char_tokens.end()) {
            token_ = it->second;
            return true;
        }
        return false;
    }
};
