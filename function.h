#pragma once

#include <cassert>
#include <memory>
#include <ostream>
#include <vector>
#include <functional>

#include "objects.h"

class Function : public Object {
public:
    std::shared_ptr<Object> Eval(std::shared_ptr<Scope>) override {
        throw std::runtime_error("Function object is not evaluable");
    }

    virtual std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                          const std::vector<std::shared_ptr<Object>>& args) = 0;

    void PrintTo(std::ostream& os) override {
        os << "<function>";
    }
};

class Closure : public Function {
public:
    Closure(std::vector<std::shared_ptr<Object>> params, std::vector<std::shared_ptr<Object>> body,
            const std::shared_ptr<Scope>& parent_scope)
        : params_(std::move(params)),
          body_(std::move(body)),
          local_scope_(std::make_shared<Scope>(parent_scope)) {
    }

    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

    void PrintTo(std::ostream& os) override {
        os << "<closure>";
    }

private:
    std::vector<std::shared_ptr<Object>> params_;
    std::vector<std::shared_ptr<Object>> body_;
    std::shared_ptr<Scope> local_scope_;
};

class NullPredicate : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 1) {
            throw std::runtime_error("null? expected 1 argument");
        }
        return std::make_shared<Boolean>(args[0] == nullptr);
    }

    void PrintTo(std::ostream& os) override {
        os << "null?";
    }
};

class NumberPredicate : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

    void PrintTo(std::ostream& os) override {
        os << "number?";
    }
};

class BooleanPredicate : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

    void PrintTo(std::ostream& os) override {
        os << "boolean?";
    }
};

class SymbolPredicate : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

    void PrintTo(std::ostream& os) override {
        os << "boolean?";
    }
};

class PairPredicate : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

    void PrintTo(std::ostream& os) override {
        os << "boolean?";
    }
};

class ListPredicate : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

    void PrintTo(std::ostream& os) override {
        os << "boolean?";
    }
};

class Plus : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class Multiplies : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class Abs : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class IntegerOp : public Function {
public:
    explicit IntegerOp(std::function<int64_t(int64_t, int64_t)> op) : op_(std::move(op)) {
    }

    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

private:
    std::function<int64_t(int64_t, int64_t)> op_;
};

class IntegerComparison : public Function {
public:
    explicit IntegerComparison(std::function<bool(int64_t, int64_t)> op) : op_(std::move(op)) {
    }

    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

private:
    std::function<int64_t(int64_t, int64_t)> op_;
};

class Not : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 1) {
            throw std::runtime_error("Not expected 1 argument");
        }
        return std::make_shared<Boolean>(args[0] != nullptr && !args[0]->IsTrue());
    }
};

class ListCreation : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        return VectorToCell(args);
    }
};

class Cons : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 2) {
            throw std::runtime_error("Cons expected 2 argument");
        }
        return std::make_shared<Cell>(args[0], args[1]);
    }
};

class Car : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 1) {
            throw std::runtime_error("Car expected 1 argument");
        }
        if (!IsCell(args[0])) {
            throw std::runtime_error("Car expected pair as an argument");
        }
        return AsCell(args[0])->GetFirst();
    }
};

class Cdr : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 1) {
            throw std::runtime_error("Cdr expected 1 argument");
        }
        if (!IsCell(args[0])) {
            throw std::runtime_error("Cdr expected pair as an argument");
        }
        return AsCell(args[0])->GetSecond();
    }
};

class SetCar : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 2) {
            throw std::runtime_error("set-car! expected 2 arguments");
        }
        auto cell = AsCell(args[0]);
        if (!cell) {
            throw std::runtime_error("set-car! expected pair as first argument");
        }
        cell->SetFirst(args[1]);
        return cell;
    }
};

class SetCdr : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 2) {
            throw std::runtime_error("set-cdr! expected 2 arguments");
        }
        auto cell = AsCell(args[0]);
        if (!cell) {
            throw std::runtime_error("set-cdr! expected pair as first argument");
        }
        cell->SetSecond(args[1]);
        return cell;
    }
};

class ListRef : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 2) {
            throw std::runtime_error("list-ref expected 2 arguments");
        }
        auto cell = AsCell(args[0]);
        if (!cell) {
            throw std::runtime_error("list-ref expected list as first argument");
        }

        auto number = AsNumber(args[1]);
        if (!number) {
            throw std::runtime_error("list-ref expected number as second argument");
        }

        auto elements = CellToVector(cell);
        auto pos = number->GetValue();
        if (pos < 0 || static_cast<size_t>(pos) >= elements.size()) {
            throw std::runtime_error("list-ref: position out of bound");
        }
        return elements[pos];
    }
};

class ListTail : public Function {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& args) override {
        if (args.size() != 2) {
            throw std::runtime_error("list-tail expected 2 arguments");
        }
        auto cell = AsCell(args[0]);
        if (!cell) {
            throw std::runtime_error("list-tail expected list as first argument");
        }

        auto number = AsNumber(args[1]);
        if (!number) {
            throw std::runtime_error("list-tail expected number as second argument");
        }

        auto elements = CellToVector(cell);
        auto pos = number->GetValue();
        if (pos < 0 || static_cast<size_t>(pos) > elements.size()) {
            throw std::runtime_error("list-tail: position out of bound");
        }
        return VectorToCell(
            std::vector<std::shared_ptr<Object>>(elements.begin() + pos, elements.end()));
    }
};
