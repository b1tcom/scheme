#pragma once

#include <string>
#include "syntax.h"
#include "scope.h"

class Scheme {
public:
    Scheme();

    std::string Evaluate(std::string expr);

private:
    std::shared_ptr<Scope> global_scope_;
};
