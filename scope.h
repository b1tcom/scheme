#pragma once

#include <memory>
#include <unordered_map>

class NameError : public std::runtime_error {
public:
    NameError(const std::string& name) : std::runtime_error("Variable \"" + name + "\" not found") {
    }
};

class Object;

class Scope {
public:
    Scope(const std::shared_ptr<Scope>& parent) : parent_(parent.get()) {
    }

    std::shared_ptr<Object> Lookup(const std::string& name) /*const*/;

    void Set(const std::string& name, const std::shared_ptr<Object>& object);

    void Update(const std::string& name, const std::shared_ptr<Object>& object);

private:
    using VariableContainer = std::unordered_map<std::string, std::shared_ptr<Object>>;
    using VariableIterator = VariableContainer::iterator;

    Scope* parent_;
    VariableContainer variables_;

    VariableIterator LookupIter(const std::string& name) /*const*/;
};
