#pragma once

#include <memory>
#include <ostream>
#include <vector>

#include "scope.h"

class Tokenizer;

class SyntaxError : public std::runtime_error {
public:
    explicit SyntaxError(const std::string& what) : std::runtime_error(what) {
    }
};

class Object : public std::enable_shared_from_this<Object> {
public:
    virtual std::shared_ptr<Object> Eval(std::shared_ptr<Scope> scope) = 0;

    virtual void PrintTo(std::ostream& os) = 0;

    virtual bool IsTrue() const {
        return true;
    }

    virtual ~Object() = default;
};

class Boolean : public Object {
public:
    static const std::string kTrueString;
    static const std::string kFalseString;

    Boolean(bool value) : value_(value) {
    }

    std::shared_ptr<Object> Eval(std::shared_ptr<Scope>) override {
        return shared_from_this();
    }

    bool IsTrue() const override {
        return value_;
    }

    void PrintTo(std::ostream& os) override;

private:
    bool value_;
};

class Number : public Object {
public:
    Number(int64_t value) : value_(value) {
    }

    int64_t GetValue() const {
        return value_;
    }

    std::shared_ptr<Object> Eval(std::shared_ptr<Scope>) override {
        return shared_from_this();
    }

    void PrintTo(std::ostream& os) override {
        os << value_;
    }

private:
    int64_t value_;
};

class Symbol : public Object {
public:
    Symbol(std::string name) : name_(std::move(name)) {
    }

    const std::string& GetName() const {
        return name_;
    }

    std::shared_ptr<Object> Eval(std::shared_ptr<Scope> scope) override;

    void PrintTo(std::ostream& os) override {
        os << name_;
    }

private:
    std::string name_;
};

class Cell : public Object {
public:
    Cell(const std::shared_ptr<Object>& first = nullptr,
         const std::shared_ptr<Object>& second = nullptr)
        : first_(first), second_(second) {
    }

    std::shared_ptr<Object> GetFirst() const {
        return first_;
    }

    std::shared_ptr<Object> GetSecond() const {
        return second_;
    }

    void SetFirst(const std::shared_ptr<Object>& object) {
        first_ = object;
    }

    void SetSecond(const std::shared_ptr<Object>& object) {
        second_ = object;
    }

    std::shared_ptr<Object> Eval(std::shared_ptr<Scope> scope) override;

    void PrintTo(std::ostream& os) override;

private:
    std::shared_ptr<Object> first_, second_;
};

std::string Print(const std::shared_ptr<Object>& object);

std::vector<std::shared_ptr<Object>> CellToVector(std::shared_ptr<Cell> cell);
std::shared_ptr<Cell> VectorToCell(const std::vector<std::shared_ptr<Object>>& elements);

bool IsNumber(const std::shared_ptr<Object>& obj);
std::shared_ptr<Number> AsNumber(const std::shared_ptr<Object>& obj);

bool IsCell(const std::shared_ptr<Object>& obj);
std::shared_ptr<Cell> AsCell(const std::shared_ptr<Object>& obj);

bool IsSymbol(const std::shared_ptr<Object>& obj);
std::shared_ptr<Symbol> AsSymbol(const std::shared_ptr<Object>& obj);

std::shared_ptr<Object> Read(Tokenizer* tokenizer);
std::shared_ptr<Object> ReadList(Tokenizer* tokenizer);
