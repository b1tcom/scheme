#pragma once

#include <memory>
#include <cassert>
#include <stdexcept>

#include "function.h"

class Syntax : public Function {
public:
    std::shared_ptr<Object> Eval(std::shared_ptr<Scope>) override {
        throw std::runtime_error("Syntax object is not evaluable");
    }

    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> /*scope*/,
                                  const std::vector<std::shared_ptr<Object>>& /*args*/) override {
        assert(false);
    }

    void PrintTo(std::ostream& os) override {
        os << "<syntax>";
    }
};

class Quote : public Syntax {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class If : public Syntax {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class Define : public Syntax {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class LambdaCreation : public Syntax {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class Set : public Syntax {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class Eval : public Syntax {
public:
    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;
};

class BooleanFold : public Syntax {
public:
    BooleanFold(std::function<bool(bool, bool)> op, bool initial) : op_(op), initial_(initial) {
    }

    std::shared_ptr<Object> Apply(std::shared_ptr<Scope> scope,
                                  const std::vector<std::shared_ptr<Object>>& args) override;

private:
    std::function<bool(bool, bool)> op_;
    bool initial_;
};
