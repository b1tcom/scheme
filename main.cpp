#include <iostream>
#include <sstream>

#include <tokenizer.h>
#include "scheme.h"

int main() {
    Scheme scheme;
    while (true) {
        std::cout << "> ";
        std::string input;
        std::getline(std::cin, input);
        try {
            std::cout << scheme.Evaluate(input) << std::endl;
        } catch (const std::exception& exception) {
            std::cout << exception.what() << std::endl;
        }
    }
}
