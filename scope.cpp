#include "scope.h"

#include "objects.h"

#include <iostream>

Scope::VariableIterator Scope::LookupIter(const std::string& name) {
    auto it = variables_.find(name);
    if (it != variables_.end()) {
        return it;
    }
    if (parent_) {
        return parent_->LookupIter(name);
    }
    throw NameError(name);
}

std::shared_ptr<Object> Scope::Lookup(const std::string& name) {
    return LookupIter(name)->second;
}

void Scope::Set(const std::string& name, const std::shared_ptr<Object>& object) {
    variables_[name] = object;
}

void Scope::Update(const std::string& name, const std::shared_ptr<Object>& object) {
    LookupIter(name)->second = object;
}
