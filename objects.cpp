#include "objects.h"
#include "syntax.h"
#include "scope.h"

#include <tokenizer.h>

#include <ostream>
#include <vector>
#include <sstream>

const std::string Boolean::kTrueString = "#t";
const std::string Boolean::kFalseString = "#f";

void Boolean::PrintTo(std::ostream& os) {
    os << (value_ ? kTrueString : kFalseString);
}

std::shared_ptr<Object> Symbol::Eval(std::shared_ptr<Scope> scope) {
    return scope->Lookup(name_);
}

void Cell::PrintTo(std::ostream& os) {
    os << "(";

    if (first_) {
        first_->PrintTo(os);
    }
    auto object = second_;
    while (object) {
        os << " ";
        if (auto cell = AsCell(object); cell) {
            cell->GetFirst()->PrintTo(os);
            object = cell->GetSecond();
        } else {
            os << ". ";
            object->PrintTo(os);
            break;
        }
    }

    os << ")";
}

std::vector<std::shared_ptr<Object>> ToVector(const std::shared_ptr<Object>& head) {
    std::vector<std::shared_ptr<Object>> elements;
    if (!head) {
        return elements;
    }

    auto cur = head;
    while (cur) {
        auto cell = AsCell(cur);
        elements.push_back(cell->GetFirst());
        cur = cell->GetSecond();
    }

    return elements;
}

std::shared_ptr<Object> Cell::Eval(std::shared_ptr<Scope> scope) {
    auto callable = first_->Eval(scope);
    auto fn = std::dynamic_pointer_cast<Function>(callable);
    auto syntax = std::dynamic_pointer_cast<Syntax>(callable);

    if (!fn && !syntax) {
        throw std::runtime_error("First element must be function or syntax");
    }

    auto args = ToVector(second_);
    if (!syntax) {
        for (auto& arg : args) {
            arg = arg->Eval(scope);
        }
    }

    return fn->Apply(scope, args);
}

bool IsNumber(const std::shared_ptr<Object>& obj) {
    return AsNumber(obj) != nullptr;
}

std::shared_ptr<Number> AsNumber(const std::shared_ptr<Object>& obj) {
    return std::dynamic_pointer_cast<Number>(obj);
}

bool IsCell(const std::shared_ptr<Object>& obj) {
    return AsCell(obj) != nullptr;
}

std::shared_ptr<Cell> AsCell(const std::shared_ptr<Object>& obj) {
    return std::dynamic_pointer_cast<Cell>(obj);
}

bool IsSymbol(const std::shared_ptr<Object>& obj) {
    return AsSymbol(obj) != nullptr;
}
std::shared_ptr<Symbol> AsSymbol(const std::shared_ptr<Object>& obj) {
    return std::dynamic_pointer_cast<Symbol>(obj);
}

template <class... Ts>
struct Overloaded : Ts... {
    using Ts::operator()...;
};
template <class... Ts>
Overloaded(Ts...)->Overloaded<Ts...>;

std::shared_ptr<Object> Read(Tokenizer* tokenizer) {
    auto token = tokenizer->GetToken();
    return std::visit(
        Overloaded{
            [](const ConstantToken& constant) -> std::shared_ptr<Object> {
                return std::make_shared<Number>(constant.value);
            },
            [](const SymbolToken& symbol) -> std::shared_ptr<Object> {
                return std::make_shared<Symbol>(symbol.name);
            },
            [&](const BracketToken& bracket) -> std::shared_ptr<Object> {
                if (bracket == BracketToken::CLOSE) {
                    throw SyntaxError("Unexpected close bracket");
                }
                return ReadList(tokenizer);
            },
            [](const DotToken&) -> std::shared_ptr<Object> {
                return std::make_shared<Symbol>(".");
            },
            [&](const QuoteToken&) -> std::shared_ptr<Object> {
                tokenizer->Next();
                return std::make_shared<Cell>(std::make_shared<Symbol>("quote"),
                                              std::make_shared<Cell>(Read(tokenizer)));
            },
            [](const auto&) -> std::shared_ptr<Object> { return std::make_shared<Number>(666); }},
        token);
}

std::shared_ptr<Object> ReadList(Tokenizer* tokenizer) {
    std::vector<std::shared_ptr<Object>> list;
    while (true) {
        if (tokenizer->IsEnd()) {
            throw SyntaxError("Expected close bracket at the end of list");
        }
        tokenizer->Next();
        auto token = tokenizer->GetToken();
        if (token == Token{BracketToken::CLOSE}) {
            break;
        }
        list.push_back(Read(tokenizer));
    }

    if (list.empty()) {
        return nullptr;
    }

    auto is_dot = [](const std::shared_ptr<Object>& object) {
        return IsSymbol(object) && AsSymbol(object)->GetName() == ".";
    };

    auto dot_count = std::count_if(list.begin(), list.end(), is_dot);
    if (dot_count > 1) {
        throw SyntaxError("List can't contain more than 1 dot");
    } else if (dot_count == 1) {
        size_t dot_pos =
            std::distance(list.begin(), std::find_if(list.begin(), list.end(), is_dot));
        if (dot_pos == 0 || dot_pos + 2 != list.size()) {
            throw SyntaxError("Invalid list with point syntax");
        }
    }

    auto root_cell = std::make_shared<Cell>(list[0]);
    auto cell = root_cell;

    for (size_t i = 1; i < list.size(); ++i) {
        if (!is_dot(list[i])) {
            auto next_cell = std::make_shared<Cell>(list[i]);
            cell->SetSecond(next_cell);
            cell = next_cell;
        } else {
            cell->SetSecond(list[i + 1]);
            break;
        }
    }
    return root_cell;
}

std::vector<std::shared_ptr<Object>> CellToVector(std::shared_ptr<Cell> cell) {
    std::vector<std::shared_ptr<Object>> elements;
    while (cell) {
        elements.push_back(cell->GetFirst());
        cell = AsCell(cell->GetSecond());
    }
    return elements;
}

std::shared_ptr<Cell> VectorToCell(const std::vector<std::shared_ptr<Object>>& elements) {
    auto root_cell = std::make_shared<Cell>();
    std::shared_ptr<Cell> cell;
    for (const auto& elem : elements) {
        if (!cell) {
            cell = root_cell;
        } else {
            auto next = std::make_shared<Cell>();
            cell->SetSecond(next);
            cell = next;
        }
        cell->SetFirst(elem);
    }

    return root_cell;
}

std::string Print(const std::shared_ptr<Object>& object) {
    if (!object) {
        return "()";
    }
    std::stringstream ss;
    object->PrintTo(ss);
    return ss.str();
}
